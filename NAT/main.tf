resource "aws_eip" "nat_ip" {
  count = var.enable_az_specific == true ? length(var.subnets_for_nat_gw) : 1
  vpc = true
}

data "aws_subnet" "selected" {
  count = var.enable_az_specific == true ? length(var.subnets_for_nat_gw) : 1
  id = var.subnets_for_nat_gw[count.index]
}

resource "aws_nat_gateway" "nat-gw" {
  count = var.enable_az_specific == true ? length(var.subnets_for_nat_gw) : 1
  allocation_id = aws_eip.nat_ip.*.id[count.index]
  subnet_id     = var.subnets_for_nat_gw[count.index]
  tags = merge(
    {
      Name = format("%s-%s", var.nat_name,data.aws_subnet.selected.*.availability_zone[count.index])
    },
    var.tags,
  )
}
