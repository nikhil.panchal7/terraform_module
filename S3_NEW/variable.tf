variable "bucket_name" {
  type        = string
  description = "The name of the bucket. If omitted, Terraform will assign a random, unique name."
  default = "new"
}

variable "acl" {
  type        = string
  description = "The canned ACL to apply to the bucket."
  default = "private"
}

variable "s3_bucket_versioning_enable" {
  type = bool
  default = false
  description = "enable s3 bucket versioning"
}

variable "block_public_acls" {
  type        = bool
  description = "Whether Amazon S3 should block public ACLs for this bucket."
}

variable "block_public_policy" {
  type        = bool
  description = "Whether Amazon S3 should block public bucket policies for this bucket. "
}

variable "ignore_public_acls" {
  type        = bool
  description = "Whether Amazon S3 should ignore public ACLs for this bucket."
}

variable "restrict_public_buckets" {
  type        = bool
  description = "Whether Amazon S3 should restrict public bucket policies for this bucket."
}

variable "versioning" {
  type        = string
  default = "Enabled"
  description = "The versioning state of the bucket should only be used when creating or importing resources that correspond to unversioned S3 buckets."
}

variable "bucket_lifecycle_config_enable" {
  type = bool
  default = false
  description = "enable bucket_lifecycle_config"
}

variable "lifecycle_id" {
  type        = string
  description = "Unique identifier for the rule."
  default = "mysql-backup"
}

variable "lifecycle_status" {
  type        = string
  default = "Enabled"
  description = "Whether the rule is currently being applied. Valid values: Enabled or Disabled"
}

variable "objects_expire_days" {
  type        = number
  default = 90
  description = "Configuration block that specifies the expiration for the lifecycle of the object days"
}

variable "transition_days_storage_class" {
  type        = number
  description = "The number of days after creation when objects are transitioned to the specified storage class."
  default = 90
}

variable "transition_storage_class" {
  type        = string
  default = "STANDARD_IA"
  description = "The class of storage used to store the object."
}

variable "policy" {
  type = any
  description = "policy for s3 bucket"
  default = ""
#   default = <<POLICY
# {
#   "Version": "2012-10-17",
#   "Statement": [
#     {
#       "Effect": "Allow",
#       "Principal": {
#         "AWS": [
#           "<Please add arn>"
#         ]
#       },
#       "Action": [
#         "s3:GetObject",
#         "s3:ListBucket",
#         "s3:PutObject"
#       ],
#       "Resource": [
#         "arn:aws:s3:::${var.bucket_name}",
#         "arn:aws:s3:::${var.bucket_name}/*"
#       ]
#     }
#   ]
# }
# POLICY  
}


variable "server_side_encryption_configuration" {
  description = "Map containing server-side encryption configuration."
  type        = any
  default     = {}
}

variable "expected_bucket_owner" {
  description = "The account ID of the expected bucket owner"
  type        = string
  default     = null
}

