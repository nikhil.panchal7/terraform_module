resource "aws_s3_bucket" "s3" {
  bucket = var.bucket_name
}

resource "aws_s3_bucket_acl" "s3_acl" {
  bucket = aws_s3_bucket.s3.id
  acl    = var.acl
}

resource "aws_s3_bucket_public_access_block" "s3_public_access" {
  bucket                  = aws_s3_bucket.s3.id
  block_public_acls       = var.block_public_acls
  block_public_policy     = var.block_public_policy
  ignore_public_acls      = var.ignore_public_acls
  restrict_public_buckets = var.restrict_public_buckets
}

resource "aws_s3_bucket_versioning" "s3_versioning" {
  count = var.s3_bucket_versioning_enable == true ? 1 : 0
  bucket = aws_s3_bucket.s3.id
  versioning_configuration {
    status = var.versioning
  }
}

resource "aws_s3_bucket_lifecycle_configuration" "bucket_lifecycle_config" {
  count = var.bucket_lifecycle_config_enable == true ? 1 : 0
  bucket = aws_s3_bucket.s3.id
  rule {
    id = var.lifecycle_id
    expiration {
      days = var.objects_expire_days
    }
    status = var.lifecycle_status
    transition {
      days          = var.transition_days_storage_class
      storage_class = var.transition_storage_class
    }
  }
}

resource "aws_s3_bucket_policy" "bucket_policy" {
  bucket = aws_s3_bucket.s3.id
  policy = var.policy
}

resource "aws_s3_bucket_server_side_encryption_configuration" "this" {
  count = length(keys(var.server_side_encryption_configuration)) > 0 ? 1 : 0
  bucket                = aws_s3_bucket.s3.id
  expected_bucket_owner = var.expected_bucket_owner

  dynamic "rule" {
    for_each = try(flatten([var.server_side_encryption_configuration["rule"]]), [])

    content {
      bucket_key_enabled = try(rule.value.bucket_key_enabled, null)

  dynamic "apply_server_side_encryption_by_default" {
    for_each = try([rule.value.apply_server_side_encryption_by_default], [])

    content {
      sse_algorithm     = apply_server_side_encryption_by_default.value.sse_algorithm
      kms_master_key_id = try(apply_server_side_encryption_by_default.value.kms_master_key_id, null)
        }
      }
    }
  }
}
