variable "tgw_name" {
  description = "Description of the EC2 Transit Gateway."
  type = string
  default = "transit-gateway"
}

variable "description" {
  description = "Description of the EC2 Transit Gateway."
  type = string
  default = ""
}

variable "dns_support" {
  description = "Whether DNS support is enabled. Valid values: disable, enable. Default value: enable."
  type = string
  default = "enable"
}

variable "amazon_side_asn" {
  description = "Private Autonomous System Number (ASN) for the Amazon side of a BGP session. The range is 64512 to 65534 for 16-bit ASNs and 4200000000 to 4294967294 for 32-bit ASNs. Default value: 64512"
  type = number
  default = 64512
}

variable "vpn_ecmp_support" {
  description = "Whether VPN Equal Cost Multipath Protocol support is enabled. Valid values: disable, enable. Default value: enable"
  type = string
  default = "enable"
}

variable "multicast_support" {
  description = "Whether Multicast support is enabled. Required to use ec2_transit_gateway_multicast_domain. Valid values: disable, enable. Default value: disable"
  type = string
  default = "disable"
}

variable "auto_accept_shared_attachments" {
  description = "Whether resource attachment requests are automatically accepted. Valid values: disable, enable. Default value: disable"
  type = string
  default = "disable"
}

variable "default_route_table_association" {
  description = "Whether resource attachments are automatically associated with the default association route table. Valid values: disable, enable. Default value: enable"
  type = string
  default = "enable"
}

variable "default_route_table_propagation" {
  description = "Whether resource attachments automatically propagate routes to the default propagation route table. Valid values: disable, enable. Default value: enable."
  type = string
  default = "enable"
}

variable "tags" {
  description = "Key-value tags for the EC2 Transit Gateway VPC Attachment."
  type = map(any)
  default = {}
}
