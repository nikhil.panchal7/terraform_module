//Create an Route53 Hosted zone - root domain
resource "aws_route53_zone" "root_domain_zone" {
  for_each = var.create_root_domain_hosted_zone ? toset(var.root_domain_names) : []
  name     = each.value

  tags = merge(
    {
      "Name" = format("%s", var.root_domain_names)
    },
    var.zone_tags,
  )
}

//Create an Route53 Hosted zone - sub domain
resource "aws_route53_zone" "sub_domain_zone" {
  for_each = { for zone in local.helper_list : zone.sub_domain => zone }
  name     = each.value.sub_domain

  tags = merge(
    {
      "Name" = format("%s", each.value.sub_domain)
    },
    var.zone_tags,
  )
}

//Add NS records of sub domain in root domain Hosted zone
resource "aws_route53_record" "sub_domain_nameservers" {
  for_each        = { for zone in local.helper_list : zone.sub_domain => zone }
  allow_overwrite = true
  name            = each.value.sub_domain
  ttl             = var.ttl
  type            = "NS"
  zone_id         = data.aws_route53_zone.root_domain_zone[each.value.root_domain].zone_id

  records = aws_route53_zone.sub_domain_zone[each.value.sub_domain].name_servers
}