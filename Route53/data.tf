//Extract root domain hosted zone ID
data "aws_route53_zone" "root_domain_zone" {
  for_each     = toset([for zone in local.helper_list : zone.root_domain])
  name         = each.value
  private_zone = false
}