variable "create_root_domain_hosted_zone" {
  type    = bool
  default = false
}

variable "root_domain_names" {
  description = "The name of the domain"
  type        = list(string)
  default     = []
}

/* Example: 
    sub_domain_zone_names = {
      "simpplr.xyz" = {
        sub_domain_name = ["t5.simpplr.xyz", "t4.simpplr.xyz"]
      }
      "cloudforbeginners.com" = {
        sub_domain_name = ["qa.cloudforbeginners.com"]
      }
    } */
variable "sub_domain_zone_names" {
  description = "The name of the domain"
  type = map(object({
    sub_domain_name = list(string)
  }))
  default = {}
}

variable "ttl" {
  description = "The TTL of the record."
  type        = number
  default     = 172800
}

variable "type" {
  description = "The record type."
  type        = string
  default     = "NS"
}

variable "zone_tags" {
  description = "Additional tags for the VPC"
  type        = map(string)
  default     = {}
}