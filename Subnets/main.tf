resource "aws_subnet" "subnet" {
  count                   = length(var.subnets_cidr)
  availability_zone       = element(var.availability_zones, count.index)
  cidr_block              = element(var.subnets_cidr, count.index)
  vpc_id                  = var.vpc_id
  tags = merge(
    {
      Name = format("%s-%s", var.subnet_name,var.availability_zones[count.index])
    },
    var.tags,
  )
}
