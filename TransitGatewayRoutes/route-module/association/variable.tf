variable "association_config" {
  type = list(object({
    transit_gateway_attachment_id_association = string
    transit_gateway_route_table_id_as = string
  }))
  description = "association config"
  default = [ {
    transit_gateway_attachment_id_association = ""
    transit_gateway_route_table_id_as = ""
  } ]
}

variable "rt_id" {
  default = ""
  type = string
}