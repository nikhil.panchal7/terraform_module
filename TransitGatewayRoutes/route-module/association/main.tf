locals {
  association_config = { for rc in var.association_config : format("%v%v", rc.transit_gateway_attachment_id_association, rc.transit_gateway_route_table_id_as == "" ? ":bh" : "") => rc }
}

resource "aws_ec2_transit_gateway_route_table_association" "default" {
  for_each                       = local.association_config
  transit_gateway_attachment_id  = each.value["transit_gateway_attachment_id_association"]
  transit_gateway_route_table_id = each.value["transit_gateway_route_table_id_as"] == "" ? var.rt_id : each.value["transit_gateway_route_table_id_as"]
}