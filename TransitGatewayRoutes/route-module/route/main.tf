locals {
  route_config = { for rc in var.route_config : format("%v%v%v%v", rc.destination_cidr_block, rc.transit_gateway_attachment_rt_id, rc.transit_gateway_route_table_id_r, rc.blackhole ? ":bh" : "") => rc }
}
resource "aws_ec2_transit_gateway_route" "default" {
  for_each                       = local.route_config
  blackhole                      = each.value["blackhole"]
  destination_cidr_block         = each.value["destination_cidr_block"]
  transit_gateway_attachment_id  = each.value["blackhole"] ? null : each.value["transit_gateway_attachment_rt_id"]
  transit_gateway_route_table_id = each.value["transit_gateway_route_table_id_r"] == "" ? var.rt_id : each.value["transit_gateway_route_table_id_r"]
}