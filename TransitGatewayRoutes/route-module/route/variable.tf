variable "route_config" {
  type = list(object({
    blackhole              = bool
    destination_cidr_block = string
    transit_gateway_attachment_rt_id = string
    transit_gateway_route_table_id_r = string
  }))
  description = "Route config"
}

variable "rt_id" {
  default = ""
  type = string
}