locals {
  propagation_config = { for rc in var.propagation_config : format("%s%s", rc.transit_gateway_attachment_id_propagation, rc.transit_gateway_route_table_id_pr == "" ? ":bh" : "") => rc }
}

resource "aws_ec2_transit_gateway_route_table_propagation" "default" {
  for_each                       = local.propagation_config
  transit_gateway_attachment_id  = each.value["transit_gateway_attachment_id_propagation"]
  transit_gateway_route_table_id = each.value["transit_gateway_route_table_id_pr"] == "" ? var.rt_id : each.value["transit_gateway_route_table_id_pr"]
  
}