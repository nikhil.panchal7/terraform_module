variable "propagation_config" {
  type = list(object({
    transit_gateway_attachment_id_propagation = string
    transit_gateway_route_table_id_pr = string
  }))
  description = "propagation config"
}

variable "rt_id" {
  default = ""
  type = string
}