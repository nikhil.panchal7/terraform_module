locals {
  transit_gateway_id = var.transit_gateway_id
}

resource "aws_ec2_transit_gateway_route_table" "default" {
  count              = var.create_transit_gateway_route_table ? 1 : 0
  transit_gateway_id = local.transit_gateway_id
  tags = merge(
    {
      Name = format("%s", var.tgw_rt_name)
    },
    var.tags,
  )
}

module "transit_gateway_route" {
  source                         = "./route-module/route"
  for_each                       = var.create_transit_gateway_route_table_association_and_propagation && var.config != null ? var.config : {}
  route_config                   = each.value["static_routes"] != null ? each.value["static_routes"] : []
  rt_id                          = aws_ec2_transit_gateway_route_table.default[0].id
}

# Allow traffic from the VPC attachments to the Transit Gateway
module "transit_gateway_association"{
  source                         = "./route-module/association"
  for_each                       = var.create_transit_gateway_route_table_association_and_propagation && var.config != null ? var.config : {}
  association_config             = each.value["association_config"] != null ? each.value["association_config"] : []
  rt_id                          = aws_ec2_transit_gateway_route_table.default[0].id
}

# Allow traffic from the Transit Gateway to the VPC attachments
# Propagations will create propagated routes
module "transit_gateway_propagation" {
  source                         = "./route-module/propagation"
  for_each                       = var.create_transit_gateway_route_table_association_and_propagation && var.config != null ? var.config : {}
  propagation_config             = each.value["propagation_config"] != null ? each.value["propagation_config"] : []
  rt_id                          = aws_ec2_transit_gateway_route_table.default[0].id
}