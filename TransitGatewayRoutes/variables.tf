variable "tgw_rt_name" {
  description = "Description of the EC2 Transit Gateway route table."
  type = string
  default = "transit-gateway-rt"
}

variable "transit_gateway_id" {
  default = ""
  type = any
  description = "write the transit gateway id"
}

variable "tags" {
  description = "Key-value tags for the EC2 Transit Gateway VPC Attachment."
  type = map(any)
  default = {}
}

variable "create_transit_gateway_route_table" {
  type        = bool
  default     = true
  description = "Whether to create a Transit Gateway Route Table. If set to `false`, an existing Transit Gateway Route Table ID must be provided in the variable `existing_transit_gateway_route_table_id`"
}

variable "create_transit_gateway_route_table_association_and_propagation" {
  type        = bool
  default     = true
  description = "Whether to create Transit Gateway Route Table associations and propagations"
}

variable "config" {
  type = map(object({
    static_routes = set(object({
      blackhole              = bool
      destination_cidr_block = string
      transit_gateway_attachment_rt_id = string
      transit_gateway_route_table_id_r = string
    }))
    association_config = set(object({
      transit_gateway_route_table_id_as = string
      transit_gateway_attachment_id_association = string
    }))
    propagation_config = set(object({
      transit_gateway_route_table_id_pr = string
      transit_gateway_attachment_id_propagation = string
    }))
  }))

  description = "Configuration for Transit Gateway routes, and subnet routes"
  default     = null
}