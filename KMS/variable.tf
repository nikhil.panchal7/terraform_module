variable "key_name" {
  description = "The name of the key alias"
  type        = string
}

variable "key_usage" {
  description = "Specifies the intended use of the key. Valid values: ENCRYPT_DECRYPT, SIGN_VERIFY, or GENERATE_VERIFY_MAC."
  type = string
  default = "ENCRYPT_DECRYPT" 
}

variable "customer_master_key_spec" {
  description = "Specifies whether the key contains a symmetric key or an asymmetric key pair and the encryption algorithms or signing algorithms that the key supports"
  type = string
  default = "SYMMETRIC_DEFAULT"
}

variable "deletion_window_in_days" {
  description = "The duration in days after which the key is deleted after destruction of the resource"
  type        = string
  default     = 30
}

variable "multi_region" {
  description = "Indicates whether the KMS key is a multi-Region (true) or regional (false) key."
  type = bool
  default = false  
}

variable "is_enabled" {
  description = "Status of key enable or disbale"
  type        = bool
  default     = true
}

variable "enable_key_rotation" {
  description = "enable_key_rotation"
  type        = bool
  default     = false
}

variable "tags" {
  description = "A map of tags to add to all resources."
  type        = map(string)
  default     = {}
}

variable "kms_policy" {
  description = "The policy of the key usage"
  type        = any
  default     = ""
}