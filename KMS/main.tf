resource "aws_kms_key" "key" {
  description              = "Key to encrypt and decrypt secret parameters"
  key_usage                = var.key_usage
  customer_master_key_spec = var.customer_master_key_spec
  policy                   = var.kms_policy
  multi_region             = var.multi_region 
  deletion_window_in_days  = var.deletion_window_in_days
  is_enabled               = var.is_enabled
  enable_key_rotation      = var.enable_key_rotation

  tags = merge(
    {
      "Name" = var.key_name
    },
    var.tags,
  )
}

resource "aws_kms_alias" "key_alias" {
  name          = "alias/${var.key_name}"
  target_key_id = aws_kms_key.key.id
}