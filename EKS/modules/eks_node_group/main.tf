resource "aws_eks_node_group" "node_groups" {
  for_each     = var.create_node_group ? var.node_groups : {}
  cluster_name = var.cluster_name
  tags = merge(
    {
      Name = format("%s-node-group", substr(each.key, 0, 45))
    },
    {
      "provisioner" = "terraform"
    },
    each.value.tags
  )
  node_group_name = substr(each.key, 0, 45)
  node_role_arn   = var.node_role_arn
  subnet_ids      = each.value.subnets
  ami_type        = each.value.ami_type
  labels          = each.value.labels
  capacity_type   = each.value.capacity_type
  force_update_version = var.force_update_version
  instance_types  = each.value.instance_type

  launch_template {
   name = aws_launch_template.your_eks_launch_template[each.key].name
   version = aws_launch_template.your_eks_launch_template[each.key].latest_version
  }

  scaling_config {
    desired_size = each.value.desired_capacity
    max_size     = each.value.max_capacity
    min_size     = each.value.min_capacity
  }

  lifecycle {
    create_before_destroy = true
    prevent_destroy       = false
    ignore_changes        = [scaling_config.0.desired_size]
  }

}

resource "aws_launch_template" "your_eks_launch_template" {
  for_each     = var.create_node_group ? var.node_groups : {}
  name = each.value.eks_launch_template_name
  description = "aws launch template for eks cluster node group"

  vpc_security_group_ids = concat([var.eks_cluster_sg], each.value.security_group_ids)

  metadata_options {
    http_endpoint               = each.value.matadata_enable == "enable" ? "enabled" : null
    http_tokens                 = each.value.matadata_enable == "enable" ? "required" : null
  }

  block_device_mappings {
    device_name = "/dev/xvda"

    ebs {
      volume_size = each.value.volume_size
      volume_type = each.value.volume_type
      encrypted   = each.value.ebs_encrypted
      kms_key_id  = each.value.ebs_kms_key_id
    }
  }

  monitoring {
    enabled = each.value.monitoring
  }

  disable_api_termination = each.value.disable_api_termination

  tag_specifications {
    resource_type = "instance"
    tags = each.value.tags
  }
}