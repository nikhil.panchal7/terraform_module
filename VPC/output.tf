output "vpc_id" {
  description = "The ID of the VPC"
  value       = aws_vpc.main.*.id[0]
}

output "igw_id" {
  description = "The ID of the VPC"
  value       = aws_internet_gateway.igw.*.id[0]
}

output "eigw_id" {
  description = "The ID of the VPC"
  value       = aws_egress_only_internet_gateway.eigw.*.id[0]
}


