variable "vpc_id" {
  description = "VPC ID in which Route table needs to be created"
  type = string
}

variable "name" {
  description = "Name of Route Table To be created "
  type        = string
}

variable "subnet_id" {
  description = "subnet id to associate with route table"
  type = list(string)
}

variable "enable_single_rt_for_single_subnet" {
  description = "enable signle route tabel or multiple route table"
  type = bool
}


variable "gateway_id" {
  description = "gate way id to attach it in table"
  type = string
}

variable "cidr" {
  description = "CIDR to which traffic will be allowed"
  type = string
}

variable "destination_cidr_block" {
  description = "CIDR to which traffic will be allowed"
  type = string
}

variable "transit_gateway_route" {
  description = "add transit gateway route"
  type = bool
}

variable "transit_gateway_id" {
  description = "gate way id to attach it in table"
  type = string
}

variable "tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
  default     = {}
}

variable "egress_only_gateway_id" {
  description = "egress only gateway id to attach it in table"
  type = string
  default = " "
}

variable "routes" {
  description = "Map of routes to be created in the route table"
  type = map(string)
  default = {"destination" : "source"}
}

variable "enable_new_routes" {
  type = bool
  default = false
}