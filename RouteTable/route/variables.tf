variable "routes" {
  description = "Map of routes to be created in the route table"
  type = map
#   default = {
#         "0.0.0.0/0" = "igw-0c5a82785d6d82970"
#         "172.16.0.0/24" = "eni-0852527640e7b366d"
#         "192.168.0.0/16" = "vgw-02f60d440b6293326"
#     }
}

variable "route_table_id" {
  default = "route table id in which routes to be added"
  type = string
}