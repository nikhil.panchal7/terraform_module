resource "aws_route" "route" {
  for_each                  = var.routes
  route_table_id            = var.route_table_id
  destination_cidr_block    = each.key
  nat_gateway_id            = can(regex("^nat-", each.value)) ? each.value : null
  gateway_id                = can(regex("^igw-", each.value)) || can(regex("^vgw-", each.value)) ? each.value : null
  vpc_peering_connection_id = can(regex("^pcx-", each.value)) ? each.value : null
  transit_gateway_id        = can(regex("^tgw-", each.value)) ? each.value : null
  vpc_endpoint_id           = can(regex("^vpce-", each.value)) ? each.value : null
  egress_only_gateway_id    = can(regex("^eigw-", each.value)) ? each.value : null
  network_interface_id      = can(regex("^eni-", each.value)) ? each.value : null
}