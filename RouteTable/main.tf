data "aws_subnet" "selected" {
  count = var.enable_single_rt_for_single_subnet == true ? length(var.subnet_id) : 1
  id = var.subnet_id[count.index]
}

resource "aws_route_table" "route_table" {
  count = var.enable_single_rt_for_single_subnet == true ? length(var.subnet_id) : 1
  vpc_id = var.vpc_id
  route {
    cidr_block = var.cidr
    gateway_id = var.gateway_id
  }

  tags = merge(
    {
      "Name" = format("%s-%s", var.name,data.aws_subnet.selected.*.availability_zone[count.index])
    },
    var.tags,
  )
}

resource "aws_route_table_association" "multi_route_table_association" {
  count          = var.enable_single_rt_for_single_subnet == true ? length(var.subnet_id) : 1
  subnet_id      = var.subnet_id[count.index]
  route_table_id = aws_route_table.route_table.*.id[count.index]
}

resource "aws_route_table_association" "single_route_table_association" {
  count          = var.enable_single_rt_for_single_subnet == false ? length(var.subnet_id) : 0 
  subnet_id      = var.subnet_id[count.index]
  route_table_id = aws_route_table.route_table.*.id[0]
}

resource "aws_route" "r" {
  count          = var.egress_only_gateway_id == " " ? 0 : length(aws_route_table.route_table.*.id)
  route_table_id              = aws_route_table.route_table.*.id[count.index]
  destination_ipv6_cidr_block = "::/0"
  egress_only_gateway_id      = var.egress_only_gateway_id
}

resource "aws_route" "transit_route" {
  count          = var.transit_gateway_route == true ? length(aws_route_table.route_table.*.id) : 0
  route_table_id              = aws_route_table.route_table.*.id[count.index]
  destination_cidr_block      = var.destination_cidr_block
  transit_gateway_id = var.transit_gateway_id
}

module "routes" {
  source = "./route"
  count           = var.enable_new_routes == true ? length(aws_route_table.route_table.*.id) : 0
  route_table_id  = aws_route_table.route_table.*.id[count.index]
  routes          = var.routes
}
