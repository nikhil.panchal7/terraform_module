output "arn" {
  value       = aws_elasticache_replication_group.redis.arn
  description = "The Amazon Resource Name (ARN) of the created ElastiCache Replication Group."
}
output "id" {
  value       = aws_elasticache_replication_group.redis.id
  description = "The ID of the ElastiCache Replication Group."
}
output "cluster_enabled" {
  value       = aws_elasticache_replication_group.redis.cluster_enabled
  description = "Indicates if cluster mode is enabled."
}
output "configuration_endpoint_address" {
  value       = aws_elasticache_replication_group.redis.configuration_endpoint_address
  description = "The address of the replication group configuration endpoint when cluster mode is enabled."
}
output "primary_endpoint_address" {
  value       = aws_elasticache_replication_group.redis.primary_endpoint_address
  description = "The address of the endpoint for the primary node in the replication group, if the cluster mode is disabled."
}
output "reader_endpoint_address" {
  value       = aws_elasticache_replication_group.redis.reader_endpoint_address
  description = "The address of the endpoint for the reader node in the replication group, if the cluster mode is disabled."
}
output "auth_token" {
  value       = random_string.auth_token[0].result
  description = "Create a random string"
}

## KMS KEY 

output "key_alias_arn" {
  description = "The arn of the key alias"
  value       = aws_kms_alias.key_alias[0].arn
}

output "key_alias_name" {
  description = "The name of the key alias"
  value       = aws_kms_alias.key_alias[0].name
}

output "key_arn" {
  description = "The arn of the key"
  value       = aws_kms_key.kms[0].arn
}

output "key_id" {
  description = "The globally unique identifier for the key"
  value       = aws_kms_key.kms[0].id
}