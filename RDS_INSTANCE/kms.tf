## KMS KEY
resource "aws_kms_key" "kms" {
  count                    = var.create_rds_kms_key_id == true ? 1 : 0
  description              = "Key to encrypt and decrypt rds storage"
  key_usage                = var.key_usage
  customer_master_key_spec = var.customer_master_key_spec
  policy                   = var.kms_policy
  multi_region             = var.multi_region 
  deletion_window_in_days  = var.deletion_window_in_days
  is_enabled               = var.is_enabled
  enable_key_rotation      = var.enable_key_rotation

  tags = merge(
    {
      "Name" = "${var.key_name}"
    },
    var.kms_key_tags,
  )
}
resource "aws_kms_alias" "key_alias" {
  count         = var.create_rds_kms_key_id == true ? 1 : 0
  name          = "alias/${var.key_name}"
  target_key_id = aws_kms_key.kms[0].id
}
