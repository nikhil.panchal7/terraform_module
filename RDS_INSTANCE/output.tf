output "rds_address" {
  value = aws_db_instance.rds_instance.address
}
output "password" {
  value = random_string.root_password.result
  description = "root password for rds cluster"
}

output "username" {
  value = local.rds_master_user_credentials.username
  description = "root username for rds cluster"
}