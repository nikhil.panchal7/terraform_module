locals {
  vpc_id         = var.vpc_id
  private_subnet_ids = var.private_subnet_ids
}

resource "aws_db_subnet_group" "rds_subnet_group" {
  name       = "${var.db_name}-subnet-group"
  subnet_ids = local.private_subnet_ids
  tags = merge(var.tags, {
    Name = "${var.db_name}-subnet-group"
  })
}
