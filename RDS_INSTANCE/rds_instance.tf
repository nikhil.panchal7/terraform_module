resource "aws_db_instance" "rds_instance" {
  db_name                  = var.db_name
  engine                   = var.engine_name
  engine_version           = var.engine_version
  identifier               = var.identifier
  username                 = local.rds_master_user_credentials.username
  password                 = local.rds_master_user_credentials.password
  db_subnet_group_name     = aws_db_subnet_group.rds_subnet_group.id
  skip_final_snapshot      = var.skip_final_snapshot
  delete_automated_backups = var.delete_automated_backups
  multi_az                 = var.multi_az
  kms_key_id               = aws_kms_key.kms[0].arn
  publicly_accessible      = var.public_access
  vpc_security_group_ids   = var.security_groups
  instance_class           = var.instance_class
  allocated_storage        = var.allocated_storage
  backup_retention_period     = var.backup_retention_period
  storage_encrypted           = var.storage_encrypted
  auto_minor_version_upgrade  = var.auto_minor_version_upgrade
  allow_major_version_upgrade = var.allow_major_version_upgrade
  max_allocated_storage       = var.max_allocated_storage
  storage_type                = var.storage_type
  license_model               = var.license_model
  deletion_protection         = var.deletion_protection
  parameter_group_name        = aws_db_parameter_group.rds_parameter_group.name

  dynamic "s3_import" {
    for_each = var.s3_import != null ? [var.s3_import] : []
    content {
      source_engine         = "mysql"
      source_engine_version = s3_import.value.source_engine_version
      bucket_name           = s3_import.value.bucket_name
      bucket_prefix         = lookup(s3_import.value, "bucket_prefix", null)
      ingestion_role        = s3_import.value.ingestion_role
    }
  }

  tags = merge(
    {
      "Name" = "${var.db_name}"
    },
    var.tags
  )
}

resource "aws_db_parameter_group" "rds_parameter_group" {
  name        = var.db_parameter_group_name
  family      = var.db_parameter_family_name
  description = "Parameter group for RDS instances."

  dynamic "parameter" {
    for_each = var.instance_parameters
    content {
      apply_method = lookup(parameter.value, "apply_method", null)
      name         = parameter.value.name
      value        = parameter.value.value
    }
  }
}