resource "aws_vpc_endpoint" "this_endpoint" {
    vpc_id = var.vpc_id
    service_name = var.service_name
    vpc_endpoint_type = var.endpoint_type
    route_table_ids = var.endpoint_type == "Gateway" ? var.route_table_ids : null
    policy = var.endpoint_policy
    security_group_ids =  var.endpoint_type == "Gateway" ? null : var.security_group_ids
    tags = merge(
    {
        "Name" = var.endpoint_name
    },
    var.tags,
)

}