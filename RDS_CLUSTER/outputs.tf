output "password" {
  value = random_string.root_password.result
  description = "root password for rds cluster"
}

output "username" {
  value = local.rds_master_user_credentials.username
  description = "root username for rds cluster"
}

output "kms_key_arn" {
  value = aws_kms_key.kms[0].arn 
}