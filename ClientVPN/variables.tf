variable "client_vpn_name" {
  description = "name of client VPN"
  type = string
  default = ""
}

variable "vpc_id" {
  description = "The ID of the VPC to associate with the Client VPN endpoint. If no security group IDs are specified in the request, the default security group for the VPC is applied."
  type        = any
  default     = ""
}

variable "vpn_port" {
  description = "The port number for the Client VPN endpoint. Valid values are 443 and 1194. Default value is 443"
  type        = number
  default     = 443

  validation {
    condition     = var.vpn_port == null ? true : contains([443, 1194], var.vpn_port)
    error_message = "Value must be 443, 1194."
  }
}

variable "description" {
  description = "A brief description of the Client VPN endpoint."
  type        = string
  default     = ""
}

variable "split_tunnel" {
  description = "Indicates whether split-tunnel is enabled on VPN endpoint. Default value is false"
  type        = bool
  default     = false
}

variable "client_cidr_block" {
  description = "The IPv4 address range, in CIDR notation, from which to assign client IP addresses. The address range cannot overlap with the local CIDR of the VPC in which the associated subnet is located, or the routes that you add manually."
  type        = any
  default     = ""
}

variable "transport_protocol" {
  description = "The transport protocol to be used by the VPN session. Default value is udp"
  type        = string
  default     = "udp"
}

variable "self_service_portal" {
  description = "Specify whether to enable the self-service portal for the Client VPN endpoint. Values can be enabled or disabled. Default value is disabled"
  type        = string
  default     = "disabled"
}

variable "session_timeout_hours" {
  description = "The maximum session duration is a trigger by which end-users are required to re-authenticate prior to establishing a VPN session. Default value is 24 - Valid values: 8 | 10 | 12 | 24"
  type        = number
  default     = 24

  validation {
    condition     = var.session_timeout_hours == null ? true : contains([8, 10, 12, 24], var.session_timeout_hours)
    error_message = "Value must be 8, 10, 12, 24."
  }
}

variable "server_certificate_arn" {
  description = "The ARN of the ACM server certificate."
  type        = any
  default     = ""
}

variable "type" {
  description = "The type of client authentication to be used. Specify certificate-authentication to use certificate-based authentication, directory-service-authentication to use Active Directory authentication, or federated-authentication to use Federated Authentication via SAML 2.0."
  type        = string
  default     = ""
}

variable "saml_provider_arn" {
  description = "The ARN of the IAM SAML identity provider if type is federated-authentication"
  type        = any
  default     = ""
}

variable "active_directory_id" {
  description = "The ID of the Active Directory to be used for authentication if type is directory-service-authentication."
  type        = any
  default     = ""
}

variable "root_certificate_chain_arn" {
  description = "The ARN of the client certificate. The certificate must be signed by a certificate authority (CA) and it must be provisioned in AWS Certificate Manager (ACM). Only necessary when type is set to certificate-authentication."
  type        = any
  default     = ""
}

variable "self_service_saml_provider_arn" {
  description = " The ARN of the IAM SAML identity provider for the self service portal if type is federated-authentication"
  type        = any
  default     = ""
}

variable "connection_log_options_enabled" {
  description = "Indicates whether connection logging is enabled."
  type        = bool
  default     = true
}

variable "cloudwatch_log_group" {
  description = "The name of the CloudWatch Logs log group."
  type        = any
  default     = ""
}

variable "cloudwatch_log_stream" {
  description = "The name of the CloudWatch Logs log stream to which the connection data is published."
  type        = any
  default     = ""
}

variable "client_connect_options_enabled" {
  description = "Indicates whether client connect options are enabled. The default is false"
  type        = bool
  default     = false
}

variable "lambda_function_arn" {
  description = "The Amazon Resource Name (ARN) of the Lambda function used for connection authorization."
  type        = any
  default     = ""
}

variable "tags" {
  description = "A mapping of tags to assign to the resource."
  type        = map(any)
  default     = {}
}

variable "security_group_id" {
  default     = []
  type        = list(string)
  description = "Optional security group id to use instead of the default created"
}

variable "associated_subnets" {
  type        = list(string)
  default = []
  description = "List of subnets to associate with the VPN endpoint"
}

variable "authorization_rules" {
  description = "List of objects describing the authorization rules for the client vpn"
  type = list(object({
    # access_group_id      = string
    authorize_all_groups = bool
    description          = string
    target_network_cidr  = string
  }))
  default = []
}

variable "additional_routes" {
  default     = []
  description = "A list of additional routes that should be attached to the Client VPN endpoint"

  type = list(object({
    destination_cidr_block = string
    description            = string
    target_vpc_subnet_id   = string
  }))
}