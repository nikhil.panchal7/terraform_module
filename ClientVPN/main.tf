locals {
  enabled = true
}

resource "aws_ec2_client_vpn_endpoint" "client_vpn_endpoint" {
  count = local.enabled ? 1 : 0
  
  vpc_id                 = var.vpc_id
  vpn_port               = var.vpn_port
  description            = var.description
  split_tunnel           = var.split_tunnel
  security_group_ids     = var.security_group_id
  client_cidr_block      = var.client_cidr_block
  transport_protocol     = var.transport_protocol
  self_service_portal    = var.self_service_portal
  session_timeout_hours  = var.session_timeout_hours
  server_certificate_arn = var.server_certificate_arn

  authentication_options {
    type                           = var.type
    saml_provider_arn              = var.saml_provider_arn
    active_directory_id            = var.active_directory_id
    root_certificate_chain_arn     = var.root_certificate_chain_arn
    self_service_saml_provider_arn = var.self_service_saml_provider_arn
  }

  connection_log_options {
    enabled               = var.connection_log_options_enabled
    cloudwatch_log_group  = var.cloudwatch_log_group
    cloudwatch_log_stream = var.cloudwatch_log_stream
  }

  client_connect_options {
    enabled             = var.client_connect_options_enabled
    lambda_function_arn = var.lambda_function_arn
  }

  tags = merge(
    {
      Name = format("%s", var.client_vpn_name)
    },
    var.tags
  )
}

resource "aws_ec2_client_vpn_network_association" "vpn_association" {
  count = local.enabled ? length(var.associated_subnets) : 0

  client_vpn_endpoint_id = join("", aws_ec2_client_vpn_endpoint.client_vpn_endpoint.*.id)
  subnet_id              = var.associated_subnets[count.index]
  security_groups        = var.security_group_id
}

resource "aws_ec2_client_vpn_authorization_rule" "default" {
  count = local.enabled ? length(var.authorization_rules) : 0

  # access_group_id        = lookup(var.authorization_rules[count.index], "access_group_id", null)
  authorize_all_groups   = lookup(var.authorization_rules[count.index], "authorize_all_groups", null)
  client_vpn_endpoint_id = join("", aws_ec2_client_vpn_endpoint.client_vpn_endpoint.*.id)
  description            = var.authorization_rules[count.index].description
  target_network_cidr    = var.authorization_rules[count.index].target_network_cidr
}

resource "aws_ec2_client_vpn_route" "default" {
  count = local.enabled ? length(var.additional_routes) : 0

  description            = try(var.additional_routes[count.index].description, null)
  destination_cidr_block = var.additional_routes[count.index].destination_cidr_block
  client_vpn_endpoint_id = join("", aws_ec2_client_vpn_endpoint.client_vpn_endpoint.*.id)
  target_vpc_subnet_id   = var.additional_routes[count.index].target_vpc_subnet_id

  depends_on = [
    aws_ec2_client_vpn_network_association.vpn_association
  ]

  timeouts {
    create = "5m"
    delete = "5m"
  }
}