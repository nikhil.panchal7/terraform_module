output "alb_dns_name" {
  description = "DNS of ALB"
  value = var.enable_alb == true ? aws_lb.alb.*.dns_name[0] : 0
}

output "alb_arn" {
  description = "ARN of alb"
  value = var.enable_alb == true ? aws_lb.alb.*.arn[0] : 0
}

output "alb_http_listener_arn" {
  description = "ARN of alb http listener"
  value = var.enable_alb == true ? aws_alb_listener.alb_http_listener.*.arn[0] : 0
}

output "alb_https_listener_arn" {
  description = "ARN of alb https listener"
  value = var.enable_alb == true ? aws_alb_listener.alb_https_listener.*.arn[0] : 0
}