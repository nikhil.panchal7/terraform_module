
resource "aws_lb_target_group" "target_group" {
  count = var.create_target_group ? 1 : 0

  name     = var.target_grp_name
  port     = var.target_group_port
  protocol = "HTTP"
  vpc_id   = var.vpc_id

  health_check {
    healthy_threshold   = var.health_check_healthy_threshold
    interval            = var.health_check_interval
    path                = var.health_check_path
    port                = var.health_check_port
    timeout             = var.health_check_timeout
    unhealthy_threshold = var.health_check_unhealthy_threshold
    matcher             = var.health_check_matcher
  }

  tags = merge(
    {
      Name = format("%s", var.target_grp_name)
    },
      var.tags
    )
}

resource "aws_lb_listener_rule" "listener_rule" {
  count        = var.create_target_group_listener ? 1 : 0
  listener_arn = var.listener_arn
  priority     = var.priority

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.target_group[count.index].arn
  }

  condition {
    path_pattern {
      values = var.path_pattern
    }
  }

  condition {
    host_header {
      values = var.host_header
    }
  }
}
