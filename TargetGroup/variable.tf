variable "tags" {
  description = "Tags to apply to resources"
  type        = map(string)
  default     = {}
}

## load balance listern rule

variable "listener_arn" {
  type        = any
  description = "ALB Listener ARN"
  default = ""
}

variable "priority" {
  type        = number
  default     = 100
  description = "Priority number for ALB listener rules"
}

variable "path_pattern" {
  type        = list(string)
  default     = ["*"]
  description = "Path pattern for path based routing"
}

variable "host_header" {
  type        = list(string)
  description = "Host header for host based routing"
  default = []
}

variable "create_target_group_listener" {
  type = bool
  default = false
  description = "create listener rule"
}

## target group

variable "target_grp_name" {
  description = "name of the asg target group"
  type = string
  default = ""
}

variable "vpc_id" {
  type = string
  description = "provide the vpc id"
  default = ""
}

variable "target_group_port" {
  type        = number
  default     = 80
  description = "Target Group Port"
}

variable "create_target_group" {
  type        = bool
  default     = true
  description = "Target group to be added or not"
}

variable "health_check_healthy_threshold" {
  description = "Number of consecutives checks before a unhealthy target is considered healthy"
  type        = number
  default     = 3
}

variable "health_check_interval" {
  description = "Seconds between health checks"
  type        = number
  default     = 10
}

variable "health_check_path" {
  type        = string
  default     = "/ping"
  description = "Health check path"
}

variable "health_check_port" {
  type        = string
  default     = "traffic-port"
  description = "Health check port"
}

variable "health_check_timeout" {
  description = "Seconds waited before a health check fails"
  type        = number
  default     = 5
}

variable "health_check_unhealthy_threshold" {
  description = "Number of consecutive checks before considering a target unhealthy"
  type        = number
  default     = 2
}

variable "health_check_matcher" {
  description = "HTTP Code(s) that result in a successful response from a target (comma delimited)"
  type        = number
  default     = 200
}