variable "vpc_id" {
  description = "vpc id to create nacl"
  type = string
}

variable "name" {
  description = "name for nacl"
  type = string
}

variable "subnet_ids" {
  description = "subnet id for which nacl is creating"
  type = list(string)
}

variable "tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
  default     = {}
}

variable "ingress_rules" {
  description = "A map of ingress rules in a network ACL. Use the key of map as the rule number."
  type        = list(map(string))
  default = [
    {
      rule_number = 100
      rule_action = "allow"
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_block  = "0.0.0.0/0"
    },
  ]
}

variable "egress_rules" {
  description = "A map of egress rules in a network ACL. Use the key of map as the rule number."
  type        = list(map(string))
  default = [
    {
      rule_number = 100
      rule_action = "allow"
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_block  = "0.0.0.0/0"
    },
  ]
}