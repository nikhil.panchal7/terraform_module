
resource "aws_network_acl" "nacl" {

  vpc_id     = var.vpc_id
  subnet_ids = var.subnet_ids
  tags = merge(
    {
      Name = var.name
    },
    var.tags,
  )

}

resource "aws_network_acl_rule" "inbound" {
  count = length(var.ingress_rules)

  network_acl_id = aws_network_acl.nacl.id

  egress          = false
  rule_number     = var.ingress_rules[count.index]["rule_number"]
  rule_action     = var.ingress_rules[count.index]["rule_action"]
  from_port       = lookup(var.ingress_rules[count.index], "from_port", null)
  to_port         = lookup(var.ingress_rules[count.index], "to_port", null)
  icmp_code       = lookup(var.ingress_rules[count.index], "icmp_code", null)
  icmp_type       = lookup(var.ingress_rules[count.index], "icmp_type", null)
  protocol        = var.ingress_rules[count.index]["protocol"]
  cidr_block      = lookup(var.ingress_rules[count.index], "cidr_block", null)
  ipv6_cidr_block = lookup(var.ingress_rules[count.index], "ipv6_cidr_block", null)
}

resource "aws_network_acl_rule" "outbound" {
  count = length(var.egress_rules)
  network_acl_id = aws_network_acl.nacl.id

  egress          = true
  rule_number     = var.egress_rules[count.index]["rule_number"]
  rule_action     = var.egress_rules[count.index]["rule_action"]
  from_port       = lookup(var.egress_rules[count.index], "from_port", null)
  to_port         = lookup(var.egress_rules[count.index], "to_port", null)
  icmp_code       = lookup(var.egress_rules[count.index], "icmp_code", null)
  icmp_type       = lookup(var.egress_rules[count.index], "icmp_type", null)
  protocol        = var.egress_rules[count.index]["protocol"]
  cidr_block      = lookup(var.egress_rules[count.index], "cidr_block", null)
  ipv6_cidr_block = lookup(var.egress_rules[count.index], "ipv6_cidr_block", null)
}