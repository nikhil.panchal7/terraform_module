resource "aws_iam_role" "role" {
  name               = var.name
  assume_role_policy = var.assume_role_policy

  dynamic "inline_policy" {
    for_each = fileset(path.root, "iam_inline_policy/*.json")
    content {
      name   = trimprefix(trim(inline_policy.value, ".json"), "iam_inline_policy/")
      policy = (file("${path.root}/${inline_policy.key}"))


    }

  }
}
