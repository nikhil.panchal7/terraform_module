output "wafid" {
  value = aws_wafv2_web_acl.main.id
}

output "wafarn" {
  value = aws_wafv2_web_acl.main.arn
}