variable "name" {
  type        = string
  description = "WAF name"
}

variable "description" {
  type        = string
  description = "WAF description"
}

variable "scope" {
  type        = string
  description = "We can select Scope of WAF it would be regional or CLOUDFRONT"
  default     = "CLOUDFRONT"
}

variable "cloudwatch_metrics_enabled" {
  type        = bool
  description = "The associated resource sends metrics to CloudWatch"
  default     = false
}

variable "metric_name" {
  type        = string
  description = "The name of the CloudWatch metric"
}

variable "sampled_requests_enabled" {
  type        = bool
  description = "AWS WAF should store a sampling of the web requests that match the rules."
  default     = false
}

variable "enable_web_acl_association" {
  type    = bool
  default = false

}

variable "resource_arn" {
  type        = string
  description = "Resource Name (ARN) of the resource to associate with the web ACL"

}