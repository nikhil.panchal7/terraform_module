output "vpc_link_id" {
  value       = aws_api_gateway_vpc_link.main.id
  description = "VPC Link Id"
}

output "vpc_link_arn" {
  value       = aws_api_gateway_vpc_link.main.arn
  description = "VPC Link arn"
}
