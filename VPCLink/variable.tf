variable "name" {
  type        = string
  description = "VPC Link Name"
}

variable "description" {
  type        = string
  description = "VPC Link description"
}

variable "target_arns" {
  type        = list(string)
  description = "Taget arns for VPC Link"
}

variable "tags" {
  description = "Additional tags"
  type        = map(string)
  default     = {}
}