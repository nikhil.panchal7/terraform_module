resource "aws_api_gateway_vpc_link" "main" {
  name        = var.name
  description = var.description
  target_arns = var.target_arns

  tags_all = var.tags
}