variable "nlb_name" {
  type        = string
  description = "Load Balancer name"
  default     = "nlb1"
}

variable "listener_ports" {
  type = map(number)
  default = {
    http  = 80
    https = 443
  }
}

variable "subnets" {
  type        = list(string)
  description = "A list of subnet ids"
}

variable "target_group_instance_id" {
  type        = set(string)
  description = "target group instance id"
}

variable "tcp_protocol" {
  type        = string
  description = "protocol type"
  default     = "TCP"
  validation {
    condition     = contains(["TCP", "UDP", "TLS"], var.tcp_protocol)
    error_message = "Valid values are \"TCP\",\"UDP\",\"TLS\"."
  }
}

variable "target_type" {
  type        = string
  description = "target type"
  default     = "instance"
}

variable "port" {
  type        = number
  description = "port required"
  default     = 80
}

variable "healthy_threshold" {
  type        = number
  description = "healthy threshold number"
  default     = 2
}

variable "unhealthy_threshold" {
  type        = number
  description = "unhealthy threshold number"
  default     = 2
}

variable "interval" {
  type        = number
  description = "interval number"
  default     = 10
}

variable "matcher" {
  type        = string
  description = "matcher"
  default     = ""
}

variable "load_balancer_type" {
  type        = string
  description = "load balancer type"
  default     = "network"
  validation {
    condition     = contains(["network"], var.load_balancer_type)
    error_message = "Valid values are \"network\"."
  }
}

variable "internal" {
  type        = bool
  description = "internal true or false"
  default     = true
}

variable "default_action_type" {
  type        = string
  description = "default action type"
  default     = "forward"
}

variable "target_group_name" {
  type        = string
  description = "target group name"
  default     = "nlb-target-group"
}

variable "target_group_port" {
  type        = number
  description = "target group port"
  default     = 80
}

variable "vpc_id" {
  type        = string
  description = "vpc id"
}

variable "tg_attachement_port" {
  type        = number
  description = "tg attachement port"
  default     = 8080
}

variable "enable_deletion_protection" {
  type        = bool
  description = "This will prevent Terraform from deleting the load balancer."
  default     = false

}

# variable "bucket" {
#   type        = string
#   description = "The S3 bucket name to store the logs in."

# }

# variable "prefix" {
#   type        = string
#   description = "S3 bucket prefix. Logs are stored in the root if not configured."

# }

# variable "enabled" {
#   type        = bool
#   description = "Boolean to enable / disable access_logs"

# }

variable "tags" {
  type        = map(string)
  description = "Additional tags"
  default     = {}
}

variable "PROVISIONER" {
  type        = string
  description = "PROVISIONER type value in form of a string"
  default     = "terraform"
}


