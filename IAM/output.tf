output "role_id" {
  value = aws_iam_role.main.id
}

output "role_arn" {
  value = aws_iam_role.main.arn
}
