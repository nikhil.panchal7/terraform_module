output "tgw_attachment_id" {
  value = aws_ec2_transit_gateway_vpc_attachment.transit_gateway_attachment.*.id
}