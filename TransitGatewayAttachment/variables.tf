variable "transit_gateway_id" {
  default = ""
  type = any
  description = "write the transit gateway id"
}

variable "tga_name" {
  description = "Description of the EC2 Transit Gateway."
  type = string
  default = "transit-gateway-attachment"
}

variable "dns_support" {
  description = "Whether DNS support is enabled. Valid values: disable, enable. Default value: enable."
  type = string
  default = "enable"
}
variable "vpc_id" {
  description = "Identifier of EC2 VPC."
  type = any
  default = ""
}

variable "subnet_ids" {
  description = "Identifiers of EC2 Subnets."
  type = list(any)
  default = []
}

variable "ipv6_support" {
  description = "Whether IPv6 support is enabled. Valid values: disable, enable. Default value: disable"
  type = string
  default = "disable"
}

variable "appliance_mode_support" {
  description = "Whether Appliance Mode support is enabled. If enabled, a traffic flow between a source and destination uses the same Availability Zone for the VPC attachment for the lifetime of that flow. Valid values: disable, enable. Default value: disable"
  type = string
  default = "disable"
}

variable "tags" {
  description = "Key-value tags for the EC2 Transit Gateway VPC Attachment."
  type = map(any)
  default = {}
}

variable "create_tgw_attachments" {
  description = "Create transit gateway attachments"
  type = bool
  default = false
}

variable "transit_gateway_default_route_table_association" {
  description = "VPC Attachment should be associated with the EC2 Transit Gateway association default route table."
  type = bool
  default = true
}

variable "transit_gateway_default_route_table_propagation" {
  description = "VPC Attachment should propagate routes with the EC2 Transit Gateway propagation default route table."
  type = bool
  default = true
}
