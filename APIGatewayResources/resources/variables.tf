variable "rest_api_id" {
  description = "API Gateway Rest API ID"
  type = any
}

variable "parent_id" {
    description = "ID of the parent API resource"
    type = any
}
variable "path_part" {
  description = "Name the Resource need to be created in API Gateway"
  type = string
}

# "arn:aws:apigateway:us-west-2:lambda:path/2015-03-31/functions/arn:aws:lambda:us-west-2:092092384707:function:${data.aws_lambda_function..function_name}:$${stageVariables.stageName}/invocations"