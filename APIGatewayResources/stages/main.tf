# Module      : Api Gateway Deployment
# Description : Terraform module to create Api Gateway Deployment resource on AWS.
resource "aws_api_gateway_deployment" "api_gateway_deployment" {
  rest_api_id       = var.rest_api_id
  stage_description = var.stage_description
  description       = var.deployment_description

  lifecycle {
    create_before_destroy = true
  }
}

# Module      : Api Gateway Stage
# Description : Terraform module to create Api Gateway Stage resource on AWS.
resource "aws_api_gateway_stage" "api_gateway_stage" {
  deployment_id = aws_api_gateway_deployment.api_gateway_deployment.id
  rest_api_id   = var.rest_api_id
  stage_name    = var.stage_name
  xray_tracing_enabled = var.xray_tracing_enabled
  variables = var.variables

  depends_on = [
    aws_api_gateway_deployment.api_gateway_deployment
  ]
}

# Module      : Api Gateway Stage Settings
# Description : Terraform module to create Api Gateway Stage Settings on AWS.
resource "aws_api_gateway_method_settings" "api_gateway_stage_settings" {
  rest_api_id = var.rest_api_id
  stage_name  = aws_api_gateway_stage.api_gateway_stage.stage_name
  method_path = var.method_path

  settings {
    metrics_enabled = var.metrics_enabled
    logging_level   = var.logging_level
    data_trace_enabled     = true
    throttling_burst_limit = var.throttling_burst_limit
    throttling_rate_limit = var.throttling_rate_limit
  }

  depends_on = [
    aws_api_gateway_stage.api_gateway_stage
  ]
}

## IAM role

resource "aws_api_gateway_account" "this" {
  cloudwatch_role_arn = "${aws_iam_role.cloudwatch.arn}"
}

resource "aws_iam_role" "cloudwatch" {
  name = "api-gateway-cloudwatch-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "apigateway.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "cloudwatch" {
  name = "api-gateway-cloudwatch-policy"
  role = "${aws_iam_role.cloudwatch.id}"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "logs:CreateLogGroup",
                "logs:CreateLogStream",
                "logs:DescribeLogGroups",
                "logs:DescribeLogStreams",
                "logs:PutLogEvents",
                "logs:GetLogEvents",
                "logs:FilterLogEvents"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}