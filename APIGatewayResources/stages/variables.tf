variable "rest_api_id" {
  description = "ID of the associated Rest API"
  type = any
}

variable "stage_description" {
  description = "Description of the API Gateway Stage"
  type = string
}

variable "deployment_description" {
  description = "Description of the API Gateway Deployment"
  type = string
}

# variable "create_before_destroy" {
#   description = "Life cycle ploicy"
#   type = bool
#   default = true
# }

variable "stage_name" {
  description = "API Gateway Stage name"
  type = string
}

variable "variables" {
  description = "Map that defines the stage variables"
  type = map(any)
  default = {}
  //map : a group of values identified by named labels, like {name = "Mabel", age = 52}, 
  //Maps/objects are represented by a pair of curly braces containing a series of <KEY> = <VALUE> pairs
}

variable "metrics_enabled" {
  description = "Whether Amazon CloudWatch metrics are enabled for this method."
  type = bool
}

variable "logging_level" {
  description = "Logging level for this method, which effects the log entries pushed to Amazon CloudWatch Logs."
  //The available levels are OFF, ERROR, and INFO.
  type = string
}

variable "throttling_burst_limit" {
  description = "Throttling burst limit. Default: -1 (throttling disabled)."
  type = number
  default = 5000
}

variable "throttling_rate_limit" {
  description = "Throttling rate limit. Default: -1 (throttling disabled)."
  type = number
  default = 10000
}

variable "method_path" {
  description = "Method path defined as {resource_path}/{http_method} for an individual method override, or */* for overriding all methods in the stage."
  //Ensure to trim any leading forward slashes in the path (e.g., trimprefix(aws_api_gateway_resource.example.path, '/')).
  type = any
  default = "*/*"
}

variable "xray_tracing_enabled" {
  description = "Whether active tracing with X-ray is enabled. Defaults to false."
  type = bool
  default = false
}
