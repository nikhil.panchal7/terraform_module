variable "properties" {
  description = "Content map of API-specific key-value pairs describing the targeted API entity." 
  //Content map of API-specific key-value pairs describing the targeted API entity. The map must be encoded as a JSON string, 
  //e.g., "{ \"description\": \"The API does …\" }". Only Swagger-compliant key-value pairs can be exported and, hence, published.
  type = any
}

variable "rest_api_id" {
  description = "ID of the associated Rest API"
  type = any
}

variable "type" {
  description = "Type of API entity to which the documentation content appliesE.g., API, METHOD or REQUEST_BODY"
  type = string
}

variable "method" {
  description = "HTTP verb of a method. The default value is * for any method.E.g POST, GET."
  type = string
  default = ""
}

variable "name" {
  description = "Name of the targeted API entity."
  type = string
  default = ""
}

variable "path" {
  description = "URL path of the target. The default value is / for the root resource."
  type = any
  default = ""
}

variable "status_code" {
  description = " HTTP status code of a response. The default value is * for any status code."
  type = number
  default = ""
}

