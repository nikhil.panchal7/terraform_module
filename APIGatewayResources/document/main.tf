# Module      : Api Gateway Documentation part
# Description : Create Api's Documentation.
resource "aws_api_gateway_documentation_part" "doc" {
  location {
    type   = var.type
    path = var.path
    method = var.method
    status_code = var.status_code
  }

  properties  = var.properties
  rest_api_id = var.rest_api_id
}