variable "name" {
  description = "Name of API Gateway Authorizer"
  type = string
}

variable "rest_api_id" {
  description = "API Gateway Rest API id"
  type = any
}

variable "authorizer_uri" {
  description = "Authorizer's Uniform Resource Identifier (URI). This must be a well-formed Lambda function URI"
  type = any
}

variable "type" {
  description = "Type of the authorizer. Possible values are TOKEN for a Lambda function using a single authorization token submitted in a custom header," 
  //REQUEST for a Lambda function using incoming request parameters, or COGNITO_USER_POOLS for using an Amazon Cognito user pool.
  type = string
}

variable "identity_source" {
  description = "Source of the identity in an incoming request. Defaults to method.request.header.Authorization."
  //For REQUEST type, this may be a comma-separated list of values, including headers, query string parameters and stage variables.
  type = string
}

variable "authorizer_result_ttl_in_seconds" {
  description = "TTL of cached authorizer results in seconds. Defaults to 300"
  type = number
}

variable "authorizer_credentials" {
  description = "Credentials required for the authorizer. To specify an IAM Role for API Gateway to assume, use the IAM Role ARN."
  type = string
  default = ""
}

