# Module      : Api Gateway Authorizers
# Description : Create Api Gateway.
resource "aws_api_gateway_authorizer" "api_gateway_authorizer" {
  name                   = var.name
  rest_api_id            = var.rest_api_id
  authorizer_uri         = var.authorizer_uri
  type                   = var.type
  identity_source        = var.identity_source
  authorizer_result_ttl_in_seconds = var.authorizer_result_ttl_in_seconds
  authorizer_credentials = var.authorizer_credentials
}
