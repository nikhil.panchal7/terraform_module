variable "rest_api_id" {
  description = "API Gateway Rest API ID"
  type = any
}

variable "resource_id" {
  description = "ID of the parent API resource"
  type = any
}

variable "http_method" {
  description = "HTTP Method (GET, POST, PUT, DELETE, HEAD, OPTIONS, ANY)"
  type = list(any)
  default = []
}

variable "authorization" {
  description = "Type of authorization used for the method (NONE, CUSTOM, AWS_IAM, COGNITO_USER_POOLS)"
  type = list(any)
  default = []
}

variable "authorizer_count" {
  description = "Number of count to create Authorizers for api."
  type = number
  default = 0
}

variable "authorization_id" {
  description = " Authorizer id to be used when the authorization is CUSTOM or COGNITO_USER_POOLS"
  type = list(any)
  default = []
}

variable "method_request_parameters" {
  description = "Map of request parameters (from the path, query string and headers) that should be passed to the integration." 
  //The boolean value indicates whether the parameter is required (true) or optional (false). 
  //For example: request_parameters = {"method.request.header.X-Some-Header" = true "method.request.querystring.some-query-param" = true} 
  //would define that the header X-Some-Header and the query string some-query-param must be provided in the request.
  type = list(any)
  default = []
}

variable "integration_http_method" {
  description = "Integration HTTP method (GET, POST, PUT, DELETE, HEAD, OPTIONs, ANY, PATCH) specifying how API Gateway will interact with the back end." 
  //Required if type is AWS, AWS_PROXY, HTTP or HTTP_PROXY. Not all methods are compatible with all AWS integrations
  type = list(any)
  default = []
}

variable "integration_type" {
  description = "Integration input's type."
  //Valid values are HTTP (for HTTP backends), MOCK (not calling any real backend), AWS (for AWS services), AWS_PROXY (for Lambda proxy integration) and HTTP_PROXY (for HTTP proxy integration). 
  //An HTTP or HTTP_PROXY integration with a connection_type of VPC_LINK is referred to as a private integration and uses a VpcLink to connect API Gateway to a network load balancer of a VPC."
  type = list(any)
  default = []
}

variable "method_response_status_code" {
  description = "HTTP status code"
  type = list(any)
  default = []
}

variable "method_response_models" {
  description = "Map of the API models used for the response's content type"
  type = list(any)
  default = []
}

variable "method_response_parameters" {
  description = "Map of response parameters that can be sent to the caller."
  //For example: response_parameters = { "method.response.header.X-Some-Header" = true } would define that the header X-Some-Header can be provided on the response.
  type = list(any)
  default = []
}

variable "integration_request_parameters" {
    description = "Map of request query string parameters and headers that should be passed to the backend responder."
    //For example: request_parameters = { "integration.request.header.X-Some-Other-Header" = "method.request.header.X-Some-Header" }"
    type = list(any)
    default = []
}

variable "integration_response_template" {
  description = "A map specifying the templates used to transform the integration response body."
  type = list(any)
  default = []
}

variable "cache_key_parameters" {
    description = "List of cache key parameters for the integration."
    type = list(any)
    default = []
    sensitive   = true
}

variable "cache_namespace" {
  description = "Integration's cache namespace."
  type = list(any)
  default = []
  sensitive   = true
}

variable "passthrough_behavior" {
  description = "The integration passthrough behavior (WHEN_NO_MATCH, WHEN_NO_TEMPLATES, NEVER). Required if request_templates is used."
  type = list(any)
  default = []
}

variable "integration_request_templates" {
  type = list(any)
  default = []
  description = "A map of the integration's request templates."
}

variable "connection_type" {
  description = "Valid values are INTERNET (default for connections through the public routable internet), and VPC_LINK"
  type = list(any)
  default = []
}

variable "connection_id" {
  description = "ID of the VpcLink used for the integration. Required if connection_type is VPC_LINK"
  type = list(any)
  default = []
}

variable "integration_response_parameters" {
  description = "Map of response parameters that can be read from the backend response."
  //For example: response_parameters = { "method.response.header.X-Some-Header" = "integration.response.header.X-Some-Other-Header" }
  type = list(any)
  default = []
}

variable "content_handling" {
  description = "How to handle request payload content type conversions. Supported values are CONVERT_TO_BINARY and CONVERT_TO_TEXT"
  type = list(any)
  default = []
}

variable "uri" {
  description = "Input's URI. Required if type is AWS, AWS_PROXY, HTTP or HTTP_PROXY"
  type = list(any)
  default = []
}