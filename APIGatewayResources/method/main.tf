# Module      : Api Gateway Method
resource "aws_api_gateway_method" "api_gateway_method"{
  count                   = length(var.http_method) > 0 ? length(var.http_method) : 0
  rest_api_id             = var.rest_api_id
  resource_id             = var.resource_id
  http_method             = element(var.http_method, count.index)
  authorization           = length(var.authorization) > 0 ? element(var.authorization, count.index) : "NONE"
  authorizer_id           = length(var.authorization_id) > 0 ? element(var.authorization_id, count.index) : null
  request_parameters      = length(var.method_request_parameters) > 0 ? element(var.method_request_parameters, count.index) : {}
}


# Module      : Api Gateway Integration
resource "aws_api_gateway_integration" "api_gateway_integration" {
  count                   = length(aws_api_gateway_method.api_gateway_method.*.id)
  rest_api_id             = var.rest_api_id
  resource_id             = var.resource_id
  http_method             = aws_api_gateway_method.api_gateway_method.*.http_method[count.index]
  integration_http_method = length(var.integration_http_method) > 0 ? element(var.integration_http_method, count.index) : null
  type                    = length(var.integration_type) > 0 ? element(var.integration_type, count.index) : ""
  uri                     = length(var.uri) > 0 ? element(var.uri, count.index) : ""
  connection_type         = length(var.connection_type) > 0 ? element(var.connection_type, count.index) : null
  connection_id           = length(var.connection_id) > 0 ? element(var.connection_id, count.index) : ""
  content_handling        = length(var.content_handling) > 0 ? element(var.content_handling, count.index) : null
  cache_key_parameters    = length(var.cache_key_parameters) > 0 ? element(var.cache_key_parameters, count.index) : []
  cache_namespace         = length(var.cache_namespace) > 0 ? element(var.cache_namespace, count.index) : ""
  request_parameters      = length(var.integration_request_parameters) > 0 ? element(var.integration_request_parameters, count.index) : {}
  request_templates       = length(var.integration_request_templates) > 0 ? element(var.integration_request_templates, count.index) : {}
  passthrough_behavior    = length(var.passthrough_behavior) > 0 ? element(var.passthrough_behavior, count.index) : null
  depends_on              = [aws_api_gateway_method.api_gateway_method]
}


# Module      : Api Gateway Method response
resource "aws_api_gateway_method_response" "api_gateway_method_response" {
  count                   = length(aws_api_gateway_method.api_gateway_method.*.id)
  rest_api_id             = var.rest_api_id
  resource_id             = var.resource_id
  http_method             = aws_api_gateway_method.api_gateway_method.*.http_method[count.index]
  status_code             = element(var.method_response_status_code, count.index)
  response_models         = length(var.method_response_models) > 0 ? element(var.method_response_models, count.index) : {}
  response_parameters     = length(var.method_response_parameters) > 0 ? element(var.method_response_parameters, count.index) : {}
  depends_on              = [aws_api_gateway_method.api_gateway_method]
}


# Module      : Api Gateway Integration Response
resource "aws_api_gateway_integration_response" "api_gateway_integration_response" {
  count                   = length(aws_api_gateway_method.api_gateway_method.*.id)
  rest_api_id             = var.rest_api_id
  resource_id             = var.resource_id
  http_method             = aws_api_gateway_method.api_gateway_method.*.http_method[count.index]
  status_code             = aws_api_gateway_method_response.api_gateway_method_response.*.status_code[count.index]
  response_parameters     = length(var.integration_response_parameters) > 0 ? element(var.integration_response_parameters, count.index) : {}
  response_templates      = length(var.integration_response_template) > 0 ? element(var.integration_response_template, count.index) : {}
  depends_on = [
  aws_api_gateway_method_response.api_gateway_method_response,
  aws_api_gateway_integration.api_gateway_integration,
  ]
}