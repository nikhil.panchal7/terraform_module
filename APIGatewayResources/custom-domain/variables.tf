variable "api_id" {
  description = "API Gateway REST API ID."
  type = any
  default = ""
}

variable "api_stage_name" {
  description = "API Gateway REST API deployment stage name."
  type = any
  default = ""
}

variable "domain_name" {
  description = "Custom domain name."
  type = any
  default = ""
}

variable "certificate_arn" {
  description = "ACM certificate ARN."
  type = any
  default = ""
}