variable "ram_name" {
  description = "Name of the resource share"
  type        = string
  default = "tgw-ram"
}

variable "allow_external_principals" {
  description = "Boolean indicating whether principals outside the AWS organization can be associated with the resource share"
  type        = bool
  default     = false
}

variable "principal" {
  description = "The principal to associate with the resource share. Possible values are an AWS account ID, an AWS Organizations Organization ARN, or an AWS Organizations Organization Unit ARN."
  type        = string
  default = ""
}

variable "principal_resource_share_arn" {
  description = "ARN of the resource share"
  type        = string
  default = ""
}

variable "resource_arn" {
  description = "ARN of the resource to associate with the RAM Resource Share"
  type        = string
  default = ""
}

variable "association_resource_share_arn" {
  description = "ARN of the resource share"
  type        = string
  default = ""
}

variable "accepter_resource_share_arn" {
  description = "ARN of the resource share"
  type        = string
  default = ""
}

variable "tags" {
  description = "Map of tags to assign to the resource share"
  type        = map(string)
  default     = {}
}

variable "create_ram" {
  description = "enable ram"
  type = bool
  default = true
}

variable "create_ram_principal_association" {
  description = "enable ram_principal_association"
  type = bool
  default = false
}

variable "create_ram_resource_association" {
  description = "enable ram_resource_association"
  type = bool
  default = false
}

variable "create_ram_resource_share_accepter" {
  description = "enable ram_resource_share_accepter"
  type = bool
  default = false
}