output "principal_association" {
  description = "Object with the AWS RAM principal association resource"
  value       = aws_ram_principal_association.this
}

output "resource_association" {
  description = "Object with the AWS RAM resource association resource"
  value       = aws_ram_resource_association.this
}

output "share_accepter" {
  description = "Object with the AWS RAM share accepter resource"
  value       = aws_ram_resource_share_accepter.this
}