data "aws_caller_identity" "this" {}

resource "aws_ram_resource_share" "this" {
  count = var.create_ram == true ? 1 : 0

  name = var.ram_name
  tags = merge(
    {
      Name = format("%s", var.ram_name)
    },
    var.tags
  )

  allow_external_principals = var.allow_external_principals
}

resource "aws_ram_principal_association" "this" {
  count = var.create_ram_principal_association == true ? 1 : 0

  principal          = var.principal
  resource_share_arn = var.principal_resource_share_arn == "" ? aws_ram_resource_share.this[count.index].arn : var.principal_resource_share_arn

}

resource "aws_ram_resource_association" "this" {
  count = var.create_ram_resource_association == true ? 1 : 0

  resource_arn       = var.resource_arn
  resource_share_arn = var.association_resource_share_arn == "" ? aws_ram_resource_share.this[count.index].arn : var.principal_resource_share_arn
}

resource "aws_ram_resource_share_accepter" "this" {
  count = var.create_ram_resource_share_accepter == true ? 1 : 0
  share_arn = var.accepter_resource_share_arn
}